import csv
import re
from copy import copy
from enum import Enum
from typing import List, Tuple

import requests
from bs4 import BeautifulSoup
from pydantic import BaseModel, conint
from xlsxwriter.workbook import Workbook

BASE_URL = "https://pravda-sotrudnikov.ru/company/"
COMPANIES = ["vayldberriz", "ozonru"]
CHECK_PAGE = 1000
OUT_FILE = "out.csv"


class Wage(Enum):
    OFFICIAL = "official"
    PART_UNOFFICIAL = "partly_unofficial"
    UNOFFICIAL = "unofficial"

    @staticmethod
    def from_str(value: str) -> "Wage":
        values = {
            "белая": Wage.OFFICIAL,
            "серая": Wage.PART_UNOFFICIAL,
            "черная": Wage.UNOFFICIAL,
        }
        return values[value]


class Market(Enum):
    HIGHER_THAN_AVG = "higher_than_avg"
    AVERAGE = "average"
    LOWER_THAN_AVG = "lower_than_avg"

    @staticmethod
    def from_str(value: str) -> "Market":
        values = {
            "ниже рынка": Market.LOWER_THAN_AVG,
            "рыночное": Market.AVERAGE,
            "выше рынка": Market.HIGHER_THAN_AVG,
        }
        return values[value]


class Impression(Enum):
    POSITIVE = "positive"
    NEGATIVE = "negative"

    @staticmethod
    def from_str(value: str) -> "Impression":
        values = {
            "не рекомендую": Impression.NEGATIVE,
            "рекомендую": Impression.POSITIVE,
        }
        return values[value]


def get_general_rating(label, value):
    class_map = {
        "Зарплата:": Wage,
        "Предложенная з/п:": Wage,
        "Соответствие рынку:": Market,
        "Соответствие з/п рынку:": Market,
        "Общее впечатление:": Impression,
    }
    cls = class_map[label]
    return cls.from_str(value)


class NotEnoughData(Exception):
    pass


class MiscRating(BaseModel):
    name: str
    value: conint(ge=1, le=5)


class ReviewerStatus(Enum):
    EMPLOYED = 'employed'
    PREVIOUSLY_EMPLOYED = 'previously_employed'
    UNEMPLOYED = 'unemployed'
    UNSPECIFIED = 'unspecified'

    @classmethod
    def from_str(cls, string: str) -> "ReviewerStatus":
        str_to_val = {
            'Сотрудник': ReviewerStatus.EMPLOYED,
            'Бывший сотрудник': ReviewerStatus.PREVIOUSLY_EMPLOYED,
            'Соискатель': ReviewerStatus.UNEMPLOYED
        }
        return str_to_val.get(string, cls.UNSPECIFIED)


class Review(BaseModel):
    date: str
    city: str
    pros: str
    cons: str
    misc_ratings: List[MiscRating]
    market: Market
    wage: Wage
    impression: Impression
    reviewer_status: ReviewerStatus
    name: str

    def to_dict(self):
        ret = copy(self.__dict__)
        for key in ["market", "wage", "impression", "reviewer_status"]:
            ret[key] = ret[key].value
        del ret["misc_ratings"]
        for rating in self.misc_ratings:
            ret[rating.name] = rating.value
        return ret

    def get_value(self, name: str):
        d = self.to_dict()
        return d.get(name, None)


def find_first_child(element, *args, **kwargs):
    children = element.findChildren(*args, **kwargs)
    first_child = next(iter(children), None)
    return first_child


def sanitize(string: str) -> str:
    return re.sub(r"[\n\t]", "", string)


def _get_text_by_class(tag, class_):
    elem = find_first_child(tag, "div", class_=class_)
    if elem is None:
        raise NotEnoughData
    text = elem.text
    return sanitize(text)


def get_date(tag) -> str:
    return _get_text_by_class(tag, "company-reviews-list-item-date")


def get_city(tag) -> str:
    text = _get_text_by_class(tag, "company-reviews-list-item-city")
    return text[len("Город: ") :]


def get_pros(tag) -> str:
    pros_container = find_first_child(
        tag, class_="col-sm-6 col-xs-12 company-reviews-list-item-text pos"
    )
    return _get_text_by_class(pros_container, "company-reviews-list-item-text-message")


def get_cons(tag) -> str:
    cons_container = find_first_child(
        tag, class_="col-sm-6 col-xs-12 company-reviews-list-item-text con"
    )
    return _get_text_by_class(cons_container, "company-reviews-list-item-text-message")


def get_general_ratings(tag) -> Tuple[str, str, str]:
    parent = find_first_child(
        tag, "div", class_="company-reviews-list-item-ratings2 clearfix"
    )
    ret = {}
    for raw_rating in parent.findChildren(
        "div", class_="company-reviews-list-item-ratings2-item"
    ):
        label = find_first_child(
            raw_rating, "span", class_="company-reviews-list-item-ratings2-item-label"
        ).text
        value = find_first_child(
            raw_rating, "span", class_="company-reviews-list-item-ratings2-item-value"
        ).text
        rating = get_general_rating(label, value)
        ret[type(rating)] = rating
    return ret


def get_misc_ratings(tag) -> List[MiscRating]:
    container = tag.find("div", class_="company-reviews-list-item-ratings")
    ratings = container.find_all("div", class_="company-reviews-list-item-ratings-item")
    ret = []
    for rating in ratings:
        name = sanitize(
            rating.find(
                "span", class_="company-reviews-list-item-ratings-item-label"
            ).text
        )
        value = rating.find(
            "span",
            class_="company-reviews-list-item-ratings-item-stars rating-autostars",
        )["data-rating"]
        ret.append(MiscRating(name=name, value=value))
    return ret


def get_reviewer_status(name) -> ReviewerStatus:
    status_search = re.search(r'\((.*)\)', name)
    if status_search is None:
        return ReviewerStatus.UNSPECIFIED
    return ReviewerStatus.from_str(sanitize(status_search.group(1)))


def get_name(tag) -> str:
    name_tag = tag.find('div', class_='company-reviews-list-item-name')
    name = name_tag.text
    return sanitize(name)


def build_review(review_tag):
    date = get_date(review_tag)
    city = get_city(review_tag)
    pros = get_pros(review_tag)
    cons = get_cons(review_tag)
    misc_ratings = get_misc_ratings(review_tag)
    name = get_name(review_tag)
    reviewer_status = get_reviewer_status(name)

    ratings = get_general_ratings(review_tag)
    try:
        market = ratings[Market]
        wage = ratings[Wage]
        impression = ratings[Impression]
    except KeyError:
        raise NotEnoughData

    return Review(
        date=date,
        city=city,
        pros=pros,
        cons=cons,
        misc_ratings=misc_ratings,
        market=market,
        wage=wage,
        impression=impression,
        name=name,
        reviewer_status=reviewer_status
    )


def parse_text(text: str):
    soup = BeautifulSoup(text, "lxml")
    reviews = soup.find_all("div", class_="company-reviews-list-item")
    ret = []
    for review in reviews:
        try:
            ret.append(build_review(review))
        except NotEnoughData:
            continue
    return ret


def get_reviews(url: str, page: int) -> List[Review]:
    params = dict(page=page)
    print("Sending request...\r", end="")
    response = requests.get(url, params=params)
    print("Got response...\r", end="")
    return parse_text(response.text)


def get_csv_keys(reviews: List[Review]) -> List[str]:
    keys = {"date", "city", "pros", "cons", "market", "wage", "impression", "reviewer_status", "name"}
    for review in reviews:
        for misc_rating in review.misc_ratings:
            keys.add(misc_rating.name)
    return list(sorted(keys))


def write_reviews_xlsx(company: str, reviews: List[Review]) -> None:
    header = get_csv_keys(reviews)
    workbook = Workbook(f"{company}.xlsx")
    worksheet = workbook.add_worksheet()

    for col, item in enumerate(header):
        worksheet.write(0, col, item)
    for line, review in enumerate(reviews, 1):
        for col, item in enumerate(header):
            value = review.get_value(item)
            if value is not None:
                worksheet.write(line, col, value)
    workbook.close()


def get_max_page(url):
    response = requests.get(url, params=dict(page=CHECK_PAGE))
    soup = BeautifulSoup(response.text, "lxml")
    lis = soup.find_all("li", class_="active")
    current_page_tag = lis[1]
    return int(current_page_tag.find("a").text)


def main():
    for company in COMPANIES:
        reviews: List[Review] = []
        url = BASE_URL + company
        max_page = get_max_page(url)
        for page in range(1, max_page + 1):
            print(f"Scraping page {page}/{max_page}")
            reviews.extend(get_reviews(url, page))
        write_reviews_xlsx(company, reviews)


if __name__ == "__main__":
    main()
